﻿namespace CST_117_Exercise_1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.instrucitonLabel = new System.Windows.Forms.Label();
            this.caliStateflag = new System.Windows.Forms.PictureBox();
            this.arizonaStateflag = new System.Windows.Forms.PictureBox();
            this.texasStateflag = new System.Windows.Forms.PictureBox();
            this.clearFlagsbutton = new System.Windows.Forms.Button();
            this.exitButton = new System.Windows.Forms.Button();
            this.stateLabel = new System.Windows.Forms.Label();
            this.showFlagsbutton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.caliStateflag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.arizonaStateflag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.texasStateflag)).BeginInit();
            this.SuspendLayout();
            // 
            // instrucitonLabel
            // 
            this.instrucitonLabel.AutoSize = true;
            this.instrucitonLabel.Location = new System.Drawing.Point(179, 48);
            this.instrucitonLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.instrucitonLabel.Name = "instrucitonLabel";
            this.instrucitonLabel.Size = new System.Drawing.Size(306, 20);
            this.instrucitonLabel.TabIndex = 0;
            this.instrucitonLabel.Text = "Cick a flag to see the name of the state.";
            // 
            // caliStateflag
            // 
            this.caliStateflag.Image = ((System.Drawing.Image)(resources.GetObject("caliStateflag.Image")));
            this.caliStateflag.Location = new System.Drawing.Point(50, 132);
            this.caliStateflag.Name = "caliStateflag";
            this.caliStateflag.Size = new System.Drawing.Size(156, 97);
            this.caliStateflag.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.caliStateflag.TabIndex = 1;
            this.caliStateflag.TabStop = false;
            this.caliStateflag.Click += new System.EventHandler(this.caliStateflag_Click);
            // 
            // arizonaStateflag
            // 
            this.arizonaStateflag.Image = ((System.Drawing.Image)(resources.GetObject("arizonaStateflag.Image")));
            this.arizonaStateflag.Location = new System.Drawing.Point(248, 132);
            this.arizonaStateflag.Name = "arizonaStateflag";
            this.arizonaStateflag.Size = new System.Drawing.Size(178, 96);
            this.arizonaStateflag.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.arizonaStateflag.TabIndex = 2;
            this.arizonaStateflag.TabStop = false;
            this.arizonaStateflag.Click += new System.EventHandler(this.arizonaStateflag_Click);
            // 
            // texasStateflag
            // 
            this.texasStateflag.Image = ((System.Drawing.Image)(resources.GetObject("texasStateflag.Image")));
            this.texasStateflag.Location = new System.Drawing.Point(470, 132);
            this.texasStateflag.Name = "texasStateflag";
            this.texasStateflag.Size = new System.Drawing.Size(168, 96);
            this.texasStateflag.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.texasStateflag.TabIndex = 3;
            this.texasStateflag.TabStop = false;
            this.texasStateflag.Click += new System.EventHandler(this.texasStateflag_Click);
            // 
            // clearFlagsbutton
            // 
            this.clearFlagsbutton.Location = new System.Drawing.Point(248, 345);
            this.clearFlagsbutton.Name = "clearFlagsbutton";
            this.clearFlagsbutton.Size = new System.Drawing.Size(185, 38);
            this.clearFlagsbutton.TabIndex = 3;
            this.clearFlagsbutton.Text = "Clear Flags and Text";
            this.clearFlagsbutton.UseVisualStyleBackColor = true;
            this.clearFlagsbutton.Click += new System.EventHandler(this.clearFlagsbutton_Click);
            // 
            // exitButton
            // 
            this.exitButton.Location = new System.Drawing.Point(511, 344);
            this.exitButton.Name = "exitButton";
            this.exitButton.Size = new System.Drawing.Size(127, 38);
            this.exitButton.TabIndex = 5;
            this.exitButton.Text = "E&xit";
            this.exitButton.UseVisualStyleBackColor = true;
            this.exitButton.Click += new System.EventHandler(this.exitButton_Click);
            // 
            // stateLabel
            // 
            this.stateLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.stateLabel.Location = new System.Drawing.Point(64, 270);
            this.stateLabel.Name = "stateLabel";
            this.stateLabel.Size = new System.Drawing.Size(532, 45);
            this.stateLabel.TabIndex = 1;
            this.stateLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // showFlagsbutton
            // 
            this.showFlagsbutton.Location = new System.Drawing.Point(50, 345);
            this.showFlagsbutton.Name = "showFlagsbutton";
            this.showFlagsbutton.Size = new System.Drawing.Size(127, 38);
            this.showFlagsbutton.TabIndex = 2;
            this.showFlagsbutton.Text = "Show Flags";
            this.showFlagsbutton.UseVisualStyleBackColor = true;
            this.showFlagsbutton.Click += new System.EventHandler(this.showFlagsbutton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(650, 395);
            this.Controls.Add(this.showFlagsbutton);
            this.Controls.Add(this.stateLabel);
            this.Controls.Add(this.exitButton);
            this.Controls.Add(this.clearFlagsbutton);
            this.Controls.Add(this.texasStateflag);
            this.Controls.Add(this.arizonaStateflag);
            this.Controls.Add(this.caliStateflag);
            this.Controls.Add(this.instrucitonLabel);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form1";
            this.Text = "State Flags";
            ((System.ComponentModel.ISupportInitialize)(this.caliStateflag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.arizonaStateflag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.texasStateflag)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label instrucitonLabel;
        private System.Windows.Forms.PictureBox caliStateflag;
        private System.Windows.Forms.PictureBox arizonaStateflag;
        private System.Windows.Forms.PictureBox texasStateflag;
        private System.Windows.Forms.Button clearFlagsbutton;
        private System.Windows.Forms.Button exitButton;
        private System.Windows.Forms.Label stateLabel;
        private System.Windows.Forms.Button showFlagsbutton;
    }
}

