﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CST_117_Exercise_1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void caliStateflag_Click(object sender, EventArgs e)
        {
            //Clicking California State Flag will show text in state label
            stateLabel.Text = "California State Flag";
        }

        private void arizonaStateflag_Click(object sender, EventArgs e)
        {
            //Clicking Arizona State Flag will show text in state label 
            stateLabel.Text = "Arizona State Flag";
        }

        private void texasStateflag_Click(object sender, EventArgs e)
        {
            //Clicking Texas State Flag will show text in state label
            stateLabel.Text = "Texas State Flag";
        }

        private void clearFlagsbutton_Click(object sender, EventArgs e)
        {
            //This whole method will make visible property of the images to invisible
            caliStateflag.Visible = false;
            arizonaStateflag.Visible = false;
            texasStateflag.Visible = false;
            //Will change text property to none in state label
            stateLabel.Text = "";

        }

        

        private void exitButton_Click(object sender, EventArgs e)
        {
            //Will close the program
            this.Close();
        }

        private void showFlagsbutton_Click(object sender, EventArgs e)
        {
            //Will change visible property of the images to visible 
            caliStateflag.Visible = true;
            arizonaStateflag.Visible = true;
            texasStateflag.Visible = true;
        }
    }
}
